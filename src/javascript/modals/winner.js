import { createElement } from "../helpers/domHelper";
import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  const title = "Winner";
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement });
}

function createWinnerDetails(fighter) {
  const { name, source } = fighter;

  const winnerDetails = createElement({
    tagName: "div",
    className: "modal-body",
  });
  const nameElement = createElement({
    tagName: "span",
    className: "fighter-name",
  });

  const attributes = { src: source };
  const imageElement = createElement({
    tagName: "img",
    className: "fighter-image",
    attributes,
  });

  nameElement.innerText = "Name: " + name + "\n";

  winnerDetails.append(nameElement, imageElement);

  return winnerDetails;
}
