export function fight(firstFighter, secondFighter) {
  const firstTurn = getRandomIntInclusive(1, 2);
  switch (firstTurn) {
    case 1:
      return battle(firstFighter, secondFighter);
    case 2:
      return battle(secondFighter, firstFighter);
  }
}

function battle(firstFighter, secondFighter) {
  let firstFighterHP = firstFighter.health;
  let secondFighterHP = secondFighter.health;
  while (true) {
    let damageFirst = getDamage(firstFighter, secondFighter);
    secondFighterHP = secondFighterHP - damageFirst;
    let damageSecond = getDamage(secondFighter, firstFighter);
    firstFighterHP = firstFighterHP - damageSecond;
    if (firstFighterHP <= 0) {
      return secondFighter;
    } else if (secondFighterHP <= 0) {
      return firstFighter;
    }
  }
}

function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomDoubleInclusive(min, max) {
  let rand = Math.random() * (max - min + 1) + min;
  return rand > 2 ? Math.floor(rand) : rand;
}

export function getDamage(attacker, enemy) {
  const damage = getHitPower(attacker) - getBlockPower(enemy);
  return Math.max(damage, 0);
}

export function getHitPower(fighter) {
  const { attack } = fighter;
  let criticalHitChance = getRandomDoubleInclusive(1, 2);
  const hitPower = attack * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter) {
  const { defense } = fighter;
  let dodgeChance = getRandomDoubleInclusive(1, 2);
  const blockPower = defense * dodgeChance;
  return blockPower;
}
